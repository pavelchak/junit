package com.pavelchak;

import com.pavelchak.Tasks.SampleTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(value=Suite.class)
@Suite.SuiteClasses(value={SampleTest.class})
public class AllTests {

}
